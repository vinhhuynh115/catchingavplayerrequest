//
//  AppDelegate.swift
//  CatchingAVPlayerRequest
//
//  Created by Vinh Huynh on 3/30/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        WebServer.shared.start()
        return true
    }
}

