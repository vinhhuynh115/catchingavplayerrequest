//
//  ViewController.swift
//  CatchingAVPlayerRequest
//
//  Created by Vinh Huynh on 3/30/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var playerView: PlayerView!
    
    private lazy var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPlayer()
        loadPlayer()
    }

    private func setupPlayer() {
        playerView.playerLayer.player = player
    }
    
    private func loadPlayer() {
        let url = URL(string: "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8")!
        let catchingURL = WebServer.shared.stream(for: url)
        let asset = AVURLAsset(url: catchingURL)
        let playerItem = AVPlayerItem(asset: asset)
        player.replaceCurrentItem(with: playerItem)
        player.play()
        print("Player is playing \(catchingURL.absoluteString)")
    }
}

